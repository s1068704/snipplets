## Exercise 1

Wäre es vielleicht präziser wenn wir die folgenden Punkte hinzufügen

![](image-20200321090238473.png)


und wie folgt schreiben:

> [...] clearly the $`L_S(h_S) = 0`$ and thus is a valid ERM algorithm. <br>
> [...]<br>
> As shown in the picture the generalization error is $`L_{D,f}(h_S) = \frac{R-R'}{R}`$.


## Exercise 2
Ich kann den letzten Schritt nicht ganz nachvollziehen, wenn ich von der vorletzten Zeile ausgehe:
```math
e^{-\epsilon m} \ge 1 - \delta
```
Falls $`1-\delta \ge \delta`$
```math
    e^{-\epsilon m} \ge \delta\\\\
    - \epsilon m \ge \log{\delta}\\\\
    - \epsilon m \ge -\log{\frac{1}{\delta}}\\\\
    -m \ge - \frac{1}{\epsilon}\log{\frac{1}{\delta}}\\\\
    m \le \frac{1}{\epsilon}\log{\frac{1}{\delta}}
```
Dadurch ist die Ungleichung genau verkehrt herum, hast du eine andere Ableitung?
